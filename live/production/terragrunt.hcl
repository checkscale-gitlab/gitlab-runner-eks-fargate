remote_state {
  backend = "s3"
  config = {
    bucket         = "leadiq-gitlab-runner-eks-fargate-backend"
    key            = "production/${path_relative_to_include()}/terraform.tfstate"
    region         = "us-west-2"
    encrypt        = true
    dynamodb_table = "leadiq-gitlab-runner-eks-fargate-production-backend"
    profile        = "leadiq"
  }
}
